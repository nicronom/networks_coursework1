import java.net.*;
import java.lang.String;
import java.util.ArrayList;

/**
 * Given a filter of range of IPv4 addresses and hostname as
 * command line arguments, this class checks whether the IPv4
 * address of the hostname matches the filter.
 * In addition, helpful messages, where possible are displayed
 * in the terminal specifying the reason for preemptive termination,
 * as well as for a match or mismatch of filter and hostname.
 * */
public class Coursework1 {

    /*
     * Container all the addresses of the provided hostname.
     * Since the hostname may not resolve to an actual address, the
     * address array is initially empty.
     */
    private static InetAddress[] hostAddresses = null;

    /*
     * Conversely the list below will contain only IPv4 addresses of the host,
     * otherwise it will be empty.
     */
    private static ArrayList<InetAddress> hostIPv4Addresses =  new ArrayList<>();

    /*
     * Attempts to resolve the hostname to IP addresses. Prints helpful message
     * if unsuccessful. Extracts a separate list of IPv4 addresses and IPv6 addresses.
     * The reason being, because we are only interested in IPv4 addresses for comparison.
     */
    private static void resolve(String hostname) {

        try{
            // all publically exposed addresses of the hostname
            hostAddresses = InetAddress.getAllByName(hostname);
            for (InetAddress address: hostAddresses) {
                if (address instanceof Inet4Address) {
                    hostIPv4Addresses.add(address);
                }
            }

            if (hostIPv4Addresses.size() == 0 && hostAddresses.length > 0) {
                System.out.println("Provided hostname is not publically exposed by an IPv4 address.");

                System.out.println("The IPv6 addresses below were found instead: ");
                for (InetAddress address: hostAddresses) {
                    System.out.println(address.toString());
                    return;
                }
            } else if (hostAddresses.length == 0) {
                System.out.println("No IP addresses were found for the given hostname: " + hostname);
                return;
            }

        } catch (UnknownHostException e) {
            System.out.println("Could not determine the ip address of hostname: " + hostname);
            System.out.println("Connection might have timed out or an address for the hostname does not exist");
            return;
        }
    }

    /*
     * Checks whether the resolved hostname IPv4 addresses matches the provided
     * filtered ip address ( or range of addresses ).
     * Returns true if a match has been found, else false is returned.
     */
    private static boolean isMatch(String filter, String hostname) {

        /* The unsigned integers or wildcards within the filter.
         * Note that both the components and wildcards have been validated beforehand.
         */
        String[] components = filter.split("[.]");

        // Variable to store all encountered wildcards
        ArrayList<Integer> indexes = getWildcardIndexes(components);

        // Holds the signed casted filter sub-components.
        int[] filterValues = new int[components.length];

        // Obtains the addresses for the host.
        resolve(hostname);

        try {
            /*
             * verify that all string components of the filter are
             * valid signed integers of the range 0-255 or a wildcard.
             */
            for (int i=0; i < components.length; i++) {

                // skip the check for unsigned integers when wildcard is found
                if (indexes != null) {
                    if (i == indexes.get(0)) {
                        break;
                    }
                }

                filterValues[i] = Integer.valueOf(components[i]);
                if (filterValues[i] < 0 || filterValues[i] > 255) {
                    System.out.println("Value must be between 0 and 255, got " + filterValues[i]
                                        + " at group " + (i+1));
                }
            }
        } catch (Exception e) {
            System.out.println("Filter parsed symbol did not match a wildcard or unsigned integer");
            return false;
        }

        try {
            // Attempt to find a match for all IPv4 addresses available for the given hostname
            for (int i=0; i < hostIPv4Addresses.size(); i++) {

                /*
                 * Flag to indicate that the address and filter are matching.
                 * When inequality is found the flag is assigned to false.
                 */
                boolean matches = true;

                // to turn the given host address into array of bytes
                byte[] bytes = hostIPv4Addresses.get(i).getAddress();

                // container for the signed integer cast representation of the address
                int[] addressValues = new int[bytes.length];

                // then cast it to array of signed integers
                for (int  j=0; j < bytes.length; j++) {
                    addressValues[j] = ((int) bytes[j] & 0xff);
                }

                /*
                 * the check will continue until the specified index,
                 * (the end of the total amount of unsigned integer groups [4] by default)
                 * */
                int upperBound = filterValues.length;

                // if any wildcard exists, then only check whether the integers match before that wildcard
                if (indexes != null) {
                    upperBound = indexes.get(0);
                }

                // check if equality holds
                for (int k = 0; k < upperBound; k++) {
                    if (filterValues[k] != addressValues[k]) {
                        matches = false;
                    }
                }

                // all integers matched
                if (matches) {
                    return true;
                }
            }
        } catch (Exception e) {         // unexpected error occurred.
            e.printStackTrace();
            return false;
        }

        // all equality comparisons failed, no match has been found
        return false;
    }

    /* Method to indicate at which indexes the wildcards were encountered.
     * Returns a list of index markers or null if no wildcards were found.
     */
    private static ArrayList<Integer> getWildcardIndexes(String[] components) {

        // Variable to store all encountered wildcards
        ArrayList<Integer> wildcardIndeces = new ArrayList<>();

        for (int i = 0; i < components.length; i++) {
            // checks if the component at index i is a wildcard
            if (components[i].equals(Character.toString('*'))) {
                wildcardIndeces.add(i);
            }
        }

        if (wildcardIndeces.size() == 0) {
            return null;
        } else {
            return wildcardIndeces;
        }
    }

    // Checks if the components have validly placed wildcards
    private static boolean hasValidWildcards(String[] components) {

        ArrayList<Integer> wildcardIndeces = getWildcardIndexes(components);

        // nothing to check, no wildcards found
        if (wildcardIndeces == null) {
            return true;
        }

        // only one wildcard found
        if (wildcardIndeces.size() == 1) {
            // it is valid if it is located in the last position of the string
            if (wildcardIndeces.get(0) == components.length-1) {
                return true;
            } else {
                System.out.println("Invalid wildcard format.");
                System.out.println("Single wildcard found and it is not the last string of the address.");
                System.out.println("Note that 128.128.128.* is a valid format, while 128.*.128.128 is invalid.");
                return false;
            }
        }

        /*
         * The loop below traverses all encountered wildcard indeces to check
         * if any non wildcard string components follows a wildcard one. This indicates
         * an invalid IPv4 address as int.wildcard.int.int format is not allowed.
         * */
        for (int i = 0; i < wildcardIndeces.size()-1; i++) {
            int j = i+1;
            if ((wildcardIndeces.get(i) + 1) != wildcardIndeces.get(j)) {
                System.out.println("Invalid wildcard format.");
                System.out.println("Wildcard was followed by a non-wildcard string.");
                System.out.println("Note that 128.128.*.* is a valid format, while 128.*.128.* is invalid.");
                return false;
            }
        }

        // finally check if the last encountered wildcard is also the last position, invalid otherwise
        if (wildcardIndeces.get(wildcardIndeces.size() - 1) != components.length - 1) {
            System.out.println("Invalid wildcard format. ");
            System.out.println("Wildcard found and it is not the last string of the address.");
            System.out.println("Note that 128.*.*.* is a valid format, while 128.*.*.128 is invalid.");
            return false;
        }

        // all scenarios were considered and no invalid wildcards were found
        return true;
    }

    // verifies that a valid filter IPv4 address was specified
    private static boolean isValidFilter(String args[]) {

        /*
         * args[0] is the filter and it should represent a valid IPv4 address or range. A valid
         * IPv4 address is a 4-byte string of 4 integers, represented by the form *.*.*.* ,
         * where * is a wildcard the value of which can take any unsigned integer (e.g. 0-255).
         * Therefore, the total length of the components separated by '.' should be exactly 4.
         */
        String[] filterComponents = args[0].split("[.]");
        if (filterComponents.length != 4) {
            System.out.println(args[0] + " is an invalid IPv4 address.");
            System.out.println("Filter supplied must contain exactly 4 unsigned integers");
            System.out.println("IPv4 address must have the format: *.*.*.*");
            System.out.println("Each '*' represents a wildcard. This can be substituted by an integer value between 0-255");
            return false;
        }


        // if the wildcards were invalid, it is not a valid IPv4 address
        if (!hasValidWildcards(filterComponents)) {
            return false;
        }

        return true;
    }

    // verifies that an appropriate number of arguments was supplied
    private static boolean hasValidArgs(String args[]) {
        // when no arguments are supplied as cla
        if (args.length == 0 || args == null) {
            System.out.println("No command line arguments supplied.");
            System.out.println("Please supply two command arguments in the format below:");
            System.out.println("filter hostname");
            return false;
        }

        // when single cla argument is supplied
        if (args.length == 1) {
            System.out.println("Only one command line argument was supplied.");
            System.out.println("Please supply two command arguments in the format below:");
            System.out.println("filter hostname");
            return false;
        }

        return true;
    }

    public static void init(String args[]) {
        /* Invoke the command line argument (cla) verifiers.
         * If any invalid state is found or an error is encountered,
         * helpful message is displayed to the user and the process terminates.
         */
        if(!hasValidArgs(args) || !isValidFilter(args)) {
            System.exit(0);
        }

        /*
         * If both command line arguments are valid, then resolved hostname ip addresses can be compared
         * to the range of allowed addresses of the filter. The result is printed to the terminal.
         */
        System.out.println(isMatch(args[0], args[1]));

    }

    // the execution starts from the main method
    public static void main(String args[]){

        // call the subroutine which will handle all relevant processes accordingly
        init(args);
    }
}
